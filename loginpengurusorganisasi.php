<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="not-ie" lang="en"> <!--<![endif]-->
<head>
	<!-- Basic Meta Tags -->
  <meta charset="utf-8">
  <title>Tugas Basis Data</title>
	
  <!--[if (gte IE 9)|!(IE)]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <![endif]--> 

  <!-- Favicon -->

  <!-- Styles -->
  <link href="css/styles.css" rel="stylesheet">
  <link href="css/bootstrap-override.css" rel="stylesheet">

  <!-- Font Avesome Styles -->
  <link href="css/font-awesome/font-awesome.css" rel="stylesheet">
	<!--[if IE 7]>
		<link href="css/font-awesome/font-awesome-ie7.min.css" rel="stylesheet">
	<![endif]-->

  <!-- FlexSlider Style -->
  <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen">

	<!-- Internet Explorer condition - HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->   

</head>       
<body>
  <!-- Header -->
  <header id="header">
    <div class="container">
      <div class="row t-container">

        <!-- Logo -->
        <div class="span3">
          <div class="logo">
            <a href="index.htm"><img src="img/logo-headeri.png" alt=""></a>
          </div>            
        </div>

        <div class="span9">
          <div class="row space60"></div>
             <nav id="nav" role="navigation">
               	<a href="#nav" title="Show navigation">Show navigation</a>
	            <a href="#" title="Hide navigation">Hide navigation</a>
	            <ul class="clearfix">
	           	<li><a href="index.htm" title="">Home</a></li>
                <li><a href="about-us.htm" title="">About Us</a></li>                
                <li><a href="#" title=""><span>Register</span></a>
  			      <ul> <!-- Submenu -->
                      <li><a href="registersponsor.php" title="">Sponsor</a></li>
                      <li><a href="registerdonatur.php" title="">Donatur</a></li>
                      <li><a href="registerrelawan.php" title="">relawan</a></li>
                      <li><a href="registerorganisasi.php" title="">Organisasi</a></li>
  		         </ul> <!-- End Submenu -->      
               </li>
                <li class="active"><a href="#" title=""><span>Login</span></a>
  			      <ul> <!-- Submenu -->
                      <li><a href="loginrelawan.html" title="">Relawan</a></li>
                      <li><a href="logindonatur.php" title="">Donatur</a></li>
                      <li><a href="loginsponsor.php" title="">Sponsor</a></li>
                      <li><a href="loginpengurusorganisasi.html" title="">Pengurus Organisasi</a></li>
  		         </ul> <!-- End Submenu -->      
               </li>
	           </ul>
          </nav>
         </div> 
      </div> 
       <div class="row space40"></div>
  </div> 
</header><!-- Header End -->
<!-- Titlebar
================================================== -->
<section id="titlebar">
	<!-- Container -->
	<div class="container">
	
		<div class="eight columns">
			<h3 class="left">Login Pengurus Organisasi</h3>
		</div>
		
		<div class="eight columns">
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li>Login</li>
					<li>Login Pengurus Organisasi</li>
				</ul>
			</nav>
		</div>

	</div>
	<!-- Container / End -->
</section>

  <!-- Content -->
   <center>
  <form action="" method="post" enctype="multipart/form-data" name="form1">
    <p>
      <label for="email_pengurus"></label>
      <input type="text" name="email_pengurus" id="email_pengurus" placeholder="Email Pengurus">
    </p>
    <p>
      <label for="password"></label>
      <input type="text" name="password" id="password" placeholder="Password">
    </p>
       
    <button class="btn"><i class="icon-ok"></i> Login</button>        
  </form>
  </center>
 
  <!-- Footer End -->

  <!-- JavaScripts -->
  <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script> 
  <script type="text/javascript" src="js/bootstrap.min.js"></script>  
  <script type="text/javascript" src="js/functions.js"></script>
  <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
  <script type="text/javascript" defer src="js/jquery.flexslider.js"></script>

</body>
</html>
  