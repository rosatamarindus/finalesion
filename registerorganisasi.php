<?php require_once('Connections/config.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// *** Redirect if username exists
$MM_flag="MM_insert";
if (isset($_POST[$MM_flag])) {
  $MM_dupKeyRedirect="registerorganisasi.php";
  $loginUsername = $_POST['email'];
  $LoginRS__query = sprintf("SELECT email FROM tb_organisasi WHERE email=%s", GetSQLValueString($loginUsername, "text"));
  mysql_select_db($database_config, $config);
  $LoginRS=mysql_query($LoginRS__query, $config) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);

  //if there is a row in the database, the username was found - can not add the requested username
  if($loginFoundUser){
    $MM_qsChar = "?";
    //append the username to the redirect page
    if (substr_count($MM_dupKeyRedirect,"?") >=1) $MM_qsChar = "&";
    $MM_dupKeyRedirect = $MM_dupKeyRedirect . $MM_qsChar ."requsername=".$loginUsername;
    header ("Location: $MM_dupKeyRedirect");
    exit;
  }
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO tb_organisasi (nama_organisasi, website, email, kecamatan, kabupaten, provinsi, kode_pos, kelurahan, tujuan_organisasi, nama_pengurus, email_pengurus, alamat_pengurus) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['nama_organisasi'], "text"),
                       GetSQLValueString($_POST['website'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['kecamatan'], "text"),
                       GetSQLValueString($_POST['kabupaten'], "text"),
                       GetSQLValueString($_POST['provinsi'], "text"),
                       GetSQLValueString($_POST['kode_pos'], "text"),
                       GetSQLValueString($_POST['kelurahan'], "text"),
                       GetSQLValueString($_POST['tujuan_organisasi'], "text"),
                       GetSQLValueString($_POST['nama_pengurus'], "text"),
                       GetSQLValueString($_POST['email_pengurus'], "text"),
                       GetSQLValueString($_POST['alamat_pengurus'], "text"));

  mysql_select_db($database_config, $config);
  $Result1 = mysql_query($insertSQL, $config) or die(mysql_error());

  $insertGoTo = "suregorganisasi.html";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="not-ie" lang="en"> <!--<![endif]-->
<head>
	<!-- Basic Meta Tags -->
  <meta charset="utf-8">
  <title>Tugas Basis Data</title>
	
  <!--[if (gte IE 9)|!(IE)]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <![endif]--> 

  <!-- Favicon -->

  <!-- Styles -->
  <link href="css/styles.css" rel="stylesheet">
  <link href="css/bootstrap-override.css" rel="stylesheet">

  <!-- Font Avesome Styles -->
  <link href="css/font-awesome/font-awesome.css" rel="stylesheet">
	<!--[if IE 7]>
		<link href="css/font-awesome/font-awesome-ie7.min.css" rel="stylesheet">
	<![endif]-->

  <!-- FlexSlider Style -->
  <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen">

	<!-- Internet Explorer condition - HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->   

</head>       
<body>
  <!-- Header -->
  <header id="header">
    <div class="container">
      <div class="row t-container">

        <!-- Logo -->
        <div class="span3">
          <div class="logo">
            <a href="index.htm"><img src="img/logo-headeri.png" alt=""></a>
          </div>            
        </div>

        <div class="span9">
          <div class="row space60"></div>
             <nav id="nav" role="navigation">
               	<a href="#nav" title="Show navigation">Show navigation</a>
	            <a href="#" title="Hide navigation">Hide navigation</a>
	            <ul class="clearfix">
	           	<li><a href="index.htm" title="">Home</a></li>
                <li><a href="about-us.htm" title="">About Us</a></li>                
                <li class="active"><a href="#" title=""><span>Register</span></a>
  			      <ul> <!-- Submenu -->
                      <li><a href="registersponsor.php" title="">Sponsor</a></li>
                      <li><a href="registerdonatur.php" title="">Donatur</a></li>
                      <li><a href="registerrelawan.php" title="">relawan</a></li>
                      <li><a href="registerorganisasi.html" title="">Organisasi</a></li>
  		         </ul> <!-- End Submenu -->      
               </li>
                <li><a href="#" title=""><span>Login</span></a>
  			      <ul> <!-- Submenu -->
                      <li><a href="loginrelawan.php" title="">Relawan</a></li>
                      <li><a href="logindonatur.php" title="">Donatur</a></li>
                      <li><a href="loginsponsor.php" title="">Sponsor</a></li>
                      <li><a href="loginpengurusorganisasi.php" title="">Pengurus Organisasi</a></li>
  		         </ul> <!-- End Submenu -->      
               </li>
	           </ul>
          </nav>
         </div> 
      </div> 
       <div class="row space40"></div>
  </div> 
</header><!-- Header End -->
<!-- Titlebar
================================================== -->
<section id="titlebar">
	<!-- Container -->
	<div class="container">
	
		<div class="eight columns">
			<h3 class="left">Register Organisasi</h3>
		</div>
		
		<div class="eight columns">
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li>Register</li>
					<li>Register Organisasi</li>
				</ul>
			</nav>
		</div>

	</div>
	<!-- Container / End -->
</section>

  <!-- Content -->
  
  
  <center>
  
  
   <font color="#FF0000">  
  <?php if(isset($_GET['requsername'])) { ?> <p class="warning">Opps..Sorry Email: <strong><?php echo $_GET['requsername']; ?></strong> sudah digunakan. Mohon Masukan Email Baru.</p> <?php } ?>
</font>

  
  
  
  <form name="form1" method="POST" action="<?php echo $editFormAction; ?>">
    <p>
      <label for="nama_organisasi"></label>
      <input type="text" name="nama_organisasi" id="nama_organisasi" placeholder="Nama Organisasi">
    </p>
    <p>
      <label for="website"></label>
      <input type="text" name="website" id="website" placeholder="Website">
    </p>
    <p>
      <label for="email"></label>
      <input type="text" name="email" id="email" placeholder="Email">
    </p>
    <p>
    <label for="kecamatan"></label>
    <input type="text" name="kecamatan" id="kecamatan" placeholder="Kecamatan">
    <p>
      <label for="kabupaten"></label>
    <input type="text" name="kabupaten" id="kabupaten" placeholder="Kabupaten / Kota">        
    <p>
      <label for="provinsi"></label>
    <input type="text" name="provinsi" id="provinsi" placeholder="Provinsi">        
    <p>
      <label for="kode_pos"></label>
    <input type="text" name="kode_pos" id="kode_pos" placeholder="Kode Pos">        
    <p>
      <label for="kelurahan"></label>
    <input type="text" name="kelurahan" id="kelurahan" placeholder="Kelurahan">        
    <p>
      <label for="tujuan_organisasi"></label>
    <input type="text" name="tujuan_organisasi" id="tujuan_organisasi" placeholder="Tujuan Organisasi">        
    <p>
      <label for="nama_pengurus"></label>
    <input type="text" name="nama_pengurus" id="nama_pengurus" placeholder="Nama Pengurus">        
    <p>
      <label for="email_pengurus"></label>
    <input type="text" name="email_pengurus" id="email_pengurus" placeholder="Email Pengurus">        
    <p>
    <p>
      <label for="alamat_pengurus"></label>
    <input type="text" name="alamat_pengurus" id="alamat_pengurus" placeholder="Alamat Pengurus">        
    <p>
    <button class="btn"><i class="icon-ok"></i> Submit</button>        
    <input type="hidden" name="MM_insert" value="form1">
    </form>
  </p>
  </center>
  
  <!-- Footer End -->

  <!-- JavaScripts -->
  <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script> 
  <script type="text/javascript" src="js/bootstrap.min.js"></script>  
  <script type="text/javascript" src="js/functions.js"></script>
  <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
  <script type="text/javascript" defer src="js/jquery.flexslider.js"></script>

</body>
</html>
  