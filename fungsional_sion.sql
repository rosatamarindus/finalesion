-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 03 Mei 2018 pada 08.11
-- Versi Server: 5.6.14
-- Versi PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `fungsional_sion`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_donasi`
--

CREATE TABLE IF NOT EXISTS `tb_donasi` (
  `id_donasi` int(11) NOT NULL AUTO_INCREMENT,
  `organisasi` varchar(50) NOT NULL,
  `jumlah_dana` int(11) NOT NULL,
  PRIMARY KEY (`id_donasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_donasi_kegiatan`
--

CREATE TABLE IF NOT EXISTS `tb_donasi_kegiatan` (
  `id_donasi_kegiatan` int(11) NOT NULL AUTO_INCREMENT,
  `organisasi` varchar(50) NOT NULL,
  `kegiatan` text NOT NULL,
  `jumlah_dana` int(11) NOT NULL,
  PRIMARY KEY (`id_donasi_kegiatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_donatur`
--

CREATE TABLE IF NOT EXISTS `tb_donatur` (
  `id_donatur` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kabupaten` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kode_pos` varchar(50) NOT NULL,
  `kelurahan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_donatur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `tb_donatur`
--

INSERT INTO `tb_donatur` (`id_donatur`, `nama`, `email`, `password`, `kecamatan`, `kabupaten`, `provinsi`, `kode_pos`, `kelurahan`) VALUES
(3, 'Paul Mccartney', 'paul@yahoo.com', 'paulpass', 'panmas', 'depok', 'jawa barat', '16433', 'mampang'),
(4, 'ringo', 'ringo@gmail', 'ringopass', 'panmas', 'depok', 'jawa barat', '16433', 'mampang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kegiatan`
--

CREATE TABLE IF NOT EXISTS `tb_kegiatan` (
  `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `dana_dibutuhkan` int(11) NOT NULL,
  `tanggal_mulai` varchar(50) NOT NULL,
  `tanggal_selesai` varchar(50) NOT NULL,
  `reward` varchar(100) NOT NULL,
  `minimal_donasi` int(11) NOT NULL,
  `maksimal_donasi` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_kegiatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_organisasi`
--

CREATE TABLE IF NOT EXISTS `tb_organisasi` (
  `id_organisasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_organisasi` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kabupaten` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kode_pos` varchar(50) NOT NULL,
  `kelurahan` varchar(50) NOT NULL,
  `tujuan_organisasi` varchar(50) NOT NULL,
  `nama_pengurus` varchar(50) NOT NULL,
  `email_pengurus` varchar(50) NOT NULL,
  `alamat_pengurus` varchar(50) NOT NULL,
  PRIMARY KEY (`id_organisasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `tb_organisasi`
--

INSERT INTO `tb_organisasi` (`id_organisasi`, `nama_organisasi`, `website`, `email`, `kecamatan`, `kabupaten`, `provinsi`, `kode_pos`, `kelurahan`, `tujuan_organisasi`, `nama_pengurus`, `email_pengurus`, `alamat_pengurus`) VALUES
(1, 'Jelajah Bebas', 'jelajahbebas.com', 'jelajah@gmail.com', 'panmas', 'Depok', 'jawa barat', '16433', 'mampang', 'Sosial', 'Andi Chekrie', 'chekrie@gmail.com', 'pancoran mas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_relawan`
--

CREATE TABLE IF NOT EXISTS `tb_relawan` (
  `id_relawan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kabupaten` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kode_pos` varchar(50) NOT NULL,
  `kelurahan` varchar(50) NOT NULL,
  `tanggal_lahir` varchar(50) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `keahlian` varchar(50) NOT NULL,
  PRIMARY KEY (`id_relawan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `tb_relawan`
--

INSERT INTO `tb_relawan` (`id_relawan`, `nama`, `email`, `password`, `kecamatan`, `kabupaten`, `provinsi`, `kode_pos`, `kelurahan`, `tanggal_lahir`, `no_hp`, `keahlian`) VALUES
(1, 'Andi Cahyadi Kusuma', 'andi@gmail.com', 'andipass', 'panmas', 'depok', 'jawa barat', '16433', 'mampang', '25-09-1988', '081310782236', 'berenang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sponsor`
--

CREATE TABLE IF NOT EXISTS `tb_sponsor` (
  `id_sponsor` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sponsor` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kabupaten` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kode_pos` varchar(50) NOT NULL,
  `kelurahan` varchar(50) NOT NULL,
  `logo` varchar(50) NOT NULL,
  PRIMARY KEY (`id_sponsor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `tb_sponsor`
--

INSERT INTO `tb_sponsor` (`id_sponsor`, `nama_sponsor`, `email`, `password`, `kecamatan`, `kabupaten`, `provinsi`, `kode_pos`, `kelurahan`, `logo`) VALUES
(1, 'olimpia', 'olimpia@gmail.com', 'olimpiapass', 'panmas', 'depok', 'jawa barat', '16433', 'mampang', '_Koala.jpg.jpg'),
(2, 'google', 'google@gmail.com', 'google', 'sawangan', 'depok', 'jawa barat', '16433', 'mampang', '_Penguins.jpg.jpg'),
(3, 'java', 'java@gmai.com', 'java', 'depok jaya', 'depok', 'jawa barat', '16433', 'mampang', '_Desert.jpg.jpg'),
(4, 'adobe', 'adobe@gmail', 'adobe', 'panams', 'depok', 'jawa barat', '16433', 'mampang', '_Jellyfish.jpg.jpg'),
(5, 'adobe', 'adobe@gmail', 'adobe', 'panams', 'depok', 'jawa barat', '16433', 'mampang', '_Jellyfish.jpg.jpg'),
(6, 'rock', 'rock@gmail.com', 'rockpass', 'panmas', 'depok', 'jawa barat', '16433', 'mampang', '_Jellyfish.jpg.jpg'),
(7, 'rock', 'rock@gmail.com', 'rockpass', 'panmas', 'depok', 'jawa barat', '16433', 'mampang', '_Jellyfish.jpg.jpg'),
(8, 'john', 'john@yahoo.com', 'johnpass', 'mampang', 'depok', 'jawa barat', '16433', 'mampang', '_Tulips.jpg.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
